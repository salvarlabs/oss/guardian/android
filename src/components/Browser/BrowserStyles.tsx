import { StyleSheet } from 'react-native';

const BrowserStyles = StyleSheet.create({
    addressBarBG: {
        backgroundColor: '#000'
    },
    addressBarTextInput: {
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        color: '#fff',
        marginTop: 16,
        marginBottom: 8,
        marginHorizontal: 16,
        borderRadius: 25,
        paddingLeft: 16,
        height: 38,
        flex: 1,
    },
    body: {
        backgroundColor: '#FFF',
        height: '100%'
    }
});

export default BrowserStyles;