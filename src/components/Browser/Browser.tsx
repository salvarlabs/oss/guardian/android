import React from 'react';
import {
    BackHandler,
    Keyboard,
    PanResponder,
    Pressable,
    SafeAreaView,
    Text,
    TextInput,
    View,
} from 'react-native';
import {
    ShouldStartLoadRequest,
    WebViewErrorEvent,
    WebViewEvent,
    WebViewMessageEvent,
    WebViewNavigation,
    WebViewScrollEvent
} from 'react-native-webview/lib/WebViewTypes';
import { WebView } from 'react-native-webview';
import Styles from './BrowserStyles';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import { FixMyType } from '../../utils/FixMyType';
import { NavigationContext } from 'navigation-react';

/**
 * Browser component for the app.
 * Based on @tutorial react-native-simple-browser by @author liron-navon
 * Rewritten by @author saulosf
 */

const Browser = ({ componentId }: FixMyType) => {
    const { stateNavigator, data } = React.useContext(NavigationContext);
    let initialURL = "https://churchofjesuschrist.org"
    const [displayedUrl, setDisplayedUrl] = React.useState(initialURL);
    const [currentUrl, setCurrentUrl] = React.useState(initialURL);
    const [title, setTitle] = React.useState("");
    const [incognito, setIncognito] = React.useState(false);
    const [canGoForward, toggleForwardCapability] = React.useState(false);
    const [canGoBack, toggleBackCapability] = React.useState(false);
    const [pageProgress, setProgress] = React.useState(0);
    const [browserConfig, setBrowserConfig] = React.useState({
        detectorTypes: 'all',
        allowStorage: true,
        allowJavascript: true,
        allowCookies: true,
        allowLocation: true,
        allowCaching: true,
        defaultSearchEngine: 'google'
    })
    const [contentOffset, setContentOffset] = React.useState(0);

    let browserRef = React.useRef(null);


    // javascript to inject into the window
    const injectedJavaScript = `
        window.ReactNativeWebView.postMessage('injected javascript works!');
        true; // note: this is required, or you'll sometimes get silent failures   
    `;

    const getBrowserConfig = React.useCallback(() => {
        if (incognito) {
            return {
                ...browserConfig,
                allowStorage: false,
                allowCookies: false,
                allowLocation: false,
                allowCaching: false
            }
        }
        return browserConfig;
    }, [browserConfig, incognito]);

    const goBack = React.useCallback(() => {
        const browserInstance = browserRef.current as WebView | null;
        if (browserInstance !== null && canGoBack) {
            browserInstance.goBack();
        }
    }, [])

    const goForward = React.useCallback(() => {
        const browserInstance = browserRef.current as WebView | null;
        if (browserInstance !== null && canGoForward) {
            browserInstance.goForward();
        }
    }, [])

    const filterRequest = React.useCallback((request: ShouldStartLoadRequest) => {
        console.log(request)
        // if (request.url.includes('g1')) {
        //   console.warn('Not loading it.')
        //   return false
        // }
        return true;
    }, [])

    const loadUrl = React.useCallback(() => {
        let newUrl = upgradeURL(displayedUrl);
        if (newUrl.toLowerCase().trim() !== displayedUrl.toLowerCase().trim()) {
            console.warn('Setting new page...')
            setDisplayedUrl(newUrl);
            setCurrentUrl(newUrl);
        }
        Keyboard.dismiss();
    }, [displayedUrl]);

    const onBrowserError = React.useCallback((syntheticEvent: WebViewErrorEvent) => {
        const { nativeEvent } = syntheticEvent;
        console.warn("Webview error: ", nativeEvent);
    }, [])

    const onBrowserLoad = React.useCallback((syntheticEvent: WebViewEvent) => {
        const { canGoForward, canGoBack, title } = syntheticEvent.nativeEvent;
        toggleForwardCapability(canGoForward);
        toggleBackCapability(canGoBack);
        setTitle(title);
    }, []);

    const onBrowserMessage = React.useCallback((event: WebViewMessageEvent) => {
        console.log('*'.repeat(10));
        console.log('Message Received: ', event.nativeEvent.data);
        console.log('*'.repeat(10))
    }, []);

    const onNavigationStateChange = React.useCallback((navState: WebViewNavigation) => {
        const { canGoForward, canGoBack, title, url } = navState;
        toggleForwardCapability(canGoForward);
        toggleBackCapability(canGoBack);
        setTitle(title);
        setDisplayedUrl(url)
        console.log("Navigation state changed and can go back is now ", canGoBack)
    }, []);

    const reload = React.useCallback(() => {
        const browserInstance = browserRef.current as WebView | null;
        if (browserInstance !== null) {
            browserInstance.reload();
        }
    }, [browserRef.current])

    const toggleIncognitoMode = React.useCallback(() => {
        setIncognito(true);
        reload();
    }, []);

    const onScroll = React.useCallback((syntheticEvent: WebViewScrollEvent) => {
        const { contentOffset } = syntheticEvent.nativeEvent

        if (contentOffset.y < 200) {
            console.log('scroll throttle: ', contentOffset.y)
            setContentOffset(contentOffset.y)
        }
    }, [])

    const panResponder = React.useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) =>
                true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) =>
                true,
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                // The user has released all touches while this view is the
                // responder. This typically means a gesture has succeeded
                if (contentOffset === 0 && gestureState.y0 < 80 && gestureState.dy > 230.0 && gestureState.vy < 0.30) {
                    reload();
                }
            },
        })
    ).current;

    const backButtonHandler = React.useCallback(() => {
        const browserInstance = browserRef.current as WebView | null;
        console.log(canGoBack)
        if (canGoBack && browserInstance !== null) {
            console.log("COULD GO BACK")
            console.log("WEBVIEW NOT NULL")
            browserInstance.goBack();
            return true;
        } else {
            console.log("WEBVIEW WAS NULL OR COULD NOT GO BACK")
            return false
        }
    }, [canGoBack, browserRef.current])

    React.useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
        return () => BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    }, [canGoBack, browserRef.current])



    React.useEffect(() => {
        if (data.path) {
            setDisplayedUrl(data.path);
            setCurrentUrl(data.path);
        }
    }, [data])

    return (
        <>
            <SafeAreaView>

                <View style={Styles.body}>
                    <View style={Styles.addressBarBG}>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                            <TextInput
                                style={Styles.addressBarTextInput}
                                value={displayedUrl}
                                onChange={e => setDisplayedUrl(e.nativeEvent.text)}
                                onEndEditing={loadUrl}
                                returnKeyType="go"
                            />
                            <Pressable
                                style={
                                    {
                                        width: 38,
                                        height: 38,
                                        backgroundColor: '#F00',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginRight: 16,
                                        marginBottom: 8
                                    }
                                }
                                onPress={() => stateNavigator.navigate('tab')}
                            >
                                <Text style={{ color: "#FFF" }}>Aba</Text>
                            </Pressable>
                        </View>
                        {
                            pageProgress > 0 && pageProgress < 1 && (
                                <ProgressBar
                                    styleAttr="Horizontal"
                                    indeterminate={false}
                                    progress={pageProgress}
                                    color="#FFFF00"
                                />
                            )
                        }

                    </View>
                    <WebView
                        cacheEnabled={browserConfig.allowCaching}
                        dataDetectorTypes={browserConfig.detectorTypes as FixMyType}
                        domStorageEnabled={browserConfig.allowStorage}
                        geolocationEnabled={browserConfig.allowLocation}
                        injectedJavaScript={injectedJavaScript}
                        javaScriptEnabled={browserConfig.allowJavascript}
                        onError={onBrowserError}
                        onLoadProgress={progress => setProgress(progress.nativeEvent.progress)}
                        onLoad={onBrowserLoad}
                        onMessage={onBrowserMessage}
                        onNavigationStateChange={onNavigationStateChange}
                        onShouldStartLoadWithRequest={filterRequest}
                        originWhitelist={['*']}
                        ref={browserRef}
                        source={{ uri: currentUrl }}
                        thirdPartyCookiesEnabled={browserConfig.allowCookies}
                        allowsFullscreenVideo
                        incognito={incognito}
                        overScrollMode="content"
                        allowsLinkPreview
                        onScroll={onScroll}
                        {...panResponder.panHandlers}
                    // userAgent={'Guardian Browser'}
                    />
                </View>
            </SafeAreaView>
        </>
    );

}

const searchEngines = {
    'google': (uri: string) => `https://www.google.com/search?q=${uri}`,
    'duckduckgo': (uri: string) => `https://duckduckgo.com/?q=${uri}`,
    'bing': (uri: string) => `https://www.bing.com/search?q=${uri}`
};

// Gets the URL and transforms it.
//
// https://www.facebook.com => https://www.facebook.com
// facebook.com => https://www.facebook.com
// facebook => https://www.google.com/search?q=facebook
function upgradeURL(uri: string, searchEngine: 'google' | 'duckduckgo' | 'bing' = 'google') {
    const isURL = uri.split(' ').length === 1 && uri.includes('.');
    if (isURL) {
        if (!uri.startsWith('http')) {
            return 'https://' + uri;
        }
        return uri;
    }
    const encodedURI = encodeURI(uri);
    return searchEngines[searchEngine](encodedURI);
}

export default Browser