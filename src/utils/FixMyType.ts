/**
 * Type used to track which places on the app that needs type fixing.
 * If in doubt, use this one.
 */
export type FixMyType = any;