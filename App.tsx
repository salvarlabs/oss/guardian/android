/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { Linking } from 'react-native';

import { StateNavigator } from 'navigation';
import { NavigationHandler } from 'navigation-react';
import { NavigationStack } from 'navigation-react-native';
import { NavigationContext } from 'navigation-react';
import Browser from './src/components/Browser/Browser';
import { FixMyType } from './src/utils/FixMyType';

// import SQLite from 'react-native-sqlcipher-storage';
// SQLite.DEBUG(true);
// SQLite.enablePromise(true);

// type Database = Object;
// var database_name = "Test.db";
// var database_key = "password";
// var bad_database_key = "bad";
// var db: Database;

const stateNavigator = new StateNavigator([
  { key: 'tab', trackCrumbTrail: true },
]);

stateNavigator.navigate('tab');


const App = (props: FixMyType) => {
  React.useEffect(() => {

    const openLink = (url: string | null) => {
      if (url) {
        console.log('URL: ', url)
        const path = url;
        console.log(path)
        const link = stateNavigator.fluent()
          .navigate('tab', { path }).url;
        stateNavigator.navigateLink(link);
      }
    };
    Linking.getInitialURL().then(openLink);
    Linking.addEventListener('url', ({ url }) => openLink(url));
    return () => {
      Linking.removeAllListeners("url");
    }
  }, []);

  return (
    <NavigationHandler stateNavigator={stateNavigator}>
      <NavigationStack />
    </NavigationHandler>
  );
};

const { tab } = stateNavigator.states;
tab.renderScene = () => <Browser />;

export default App;